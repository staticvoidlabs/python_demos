
# API-Key yQlKmmNDyMR2vU0WF9oHZkMtZTBVW4Bvd511OJtLdsfb0wtkESWx0Q1mVi49f0GV

import sys
import requests


api_token = 'yQlKmmNDyMR2vU0WF9oHZkMtZTBVW4Bvd511OJtLdsfb0wtkESWx0Q1mVi49f0GV__'
api_url_base = 'https://gateway.sms77.io/api/sms'


def SendMessage(payload):

    response = requests.post(url=api_url_base, data=payload)

    if response.status_code == 200:
        return response.text
    else:
        return None




# --- MAIN Start ------------------------------------------------------


if len(sys.argv) > 2:

    tmpMsgType = sys.argv[1]
    tmpMsgReceiver = sys.argv[2]

    if tmpMsgType is not None and tmpMsgType == "001":
        tmpMsgText = "Kommst du mal bitte?"
    elif tmpMsgType is not None and tmpMsgType == "002":
        tmpMsgText = "Ich brauche noch etwas..."
    else:
        exit("Non valid MsgType.")

    tmpPayload = {'p': api_token, 'to': tmpMsgReceiver, 'text': tmpMsgText, 'type': 'economy', 'from': 'Python-Demo'}
    tmpResponseContent = SendMessage(tmpPayload)

    if tmpResponseContent is not None:
        print("Here's your info: ", tmpResponseContent)
    else:
        print('[!] Request Failed')

else:
    print("Command line arguments missing.")








# --- MAIN End ------------------------------------------------------




# smsio return codes:
#
#   100	> SMS wurde erfolgreich verschickt
#   101	> Versand an mindestens einen Empfänger fehlgeschlagen
#   201	> Absender ungültig. Erlaubt sind max 11 alphanumerische oder 16 numerische Zeichen.
#   202	> Empfängernummer ungültig
#   300	> Variable p ist nicht angeben
#   301	> Variable to nicht gesetzt
#   304	> Variable type nicht gesetzt
#   305	> Variable text nicht gesetzt
#   400	> type ungültig. Siehe erlaubte Werte oben.
#   401	> Variable text ist zu lang
#   402	> Reloadsperre – diese SMS wurde bereits innerhalb der letzten 180 Sekunden verschickt
#   403	> Maximales Limit pro Tag für diese Nummer erreicht.
#   500	> Zu wenig Guthaben vorhanden.
#   600	> Carrier Zustellung misslungen
#   700	> Unbekannter Fehler
#   900	> Authentifizierung ist fehlgeschlagen. Bitte Benutzer und Api Key prüfen
#   902	> http API für diesen Account deaktiviert
#   903	> Server IP ist falsch
#   11	> SMS Carrier temporär nicht verfügbar

