
import json
import requests

api_token = 'your_api_token'
api_url_base = 'https://api.digitalocean.com/v2/'



# ---------------------------
# Methods

def get_account_info():

    api_url = '{0}account'.format(api_url_base)
    
    headers = {'Content-Type': 'application/json',
               'Authorization': 'Bearer {0}'.format(api_token)}

    payload = {'schluessel1': 'wert1', 'schluessel2': 'wert2'}

    response1 = requests.get(api_url, headers=headers)
    response2 = requests.get("http://httpbin.org/get", params=payload)
    response3 = requests.post(api_url, data=json.dumps(payload), headers=headers)


    if response1.status_code == 200:
        return json.loads(response1.content.decode('utf-8'))
    else:
        return None

# ---------------------------

account_info = get_account_info()

if account_info is not None:
    print("Here's your info: ")
    for k, v in account_info['account'].items():
        print('{0}:{1}'.format(k, v))

else:
    print('[!] Request Failed')


               